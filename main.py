"""
GCS Cloud function template

Relevant links:

Writing functions:
    https://cloud.google.com/functions/docs/writing/background#functions-writing-background-hello-storage-python

Configuring the framework:
    https://cloud.google.com/functions/docs/running/function-frameworks

Curl commands for invoking function:
    https://cloud.google.com/functions/docs/running/calling#background-function-curl-tabs-storage

Running the framework:
    functions_framework --target=YOUR_FUNCTION_NAME --signature-type=SIGNATURE_TYPE
    
    (The signature type used by your function. Can be one of http (the default), event, or cloudevent.)
"""


def main(event, context):
    """Specify main as the function to be called when Cloud Function is deployed.
    """

    print("\n---------------")
    
    for key, value in event.items():
        print("event.items(): Key:",key, " Value:", value)

    blob = event['name']
    print("Blob:", blob)
    bucket = event['bucket']
    print("Bucket:", bucket)

    print(type(context))
    print("Context event ID:", context.event_id)
    print("Context event type:", context.event_type)
    print("Context event timestamp:", context.timestamp)

    for key, value in context.resource.items():
        print("context.resource.items(): Key:",key," Value:", value)
    
    print("\n---------------")

if __name__ == '__main__':
    main("", "")
