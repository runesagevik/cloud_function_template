"""
Module: gcp
Functions, methods and datastructures for working with gcs
"""
from google.cloud import storage


class Gcp:
    """
    Class for operating on gcp and gcp objects
    """

    def __init__(self, bucket_name):
        """Set up variables needed for gcp operations

        Args:
            bucket_name (str): Name of gcp bucket to use
        """
        self._sc = storage.Client()
        self._bucket_name = bucket_name
        self._bucket = self._sc.get_bucket(self._bucket_name)

    def blob_exists(self, blob_name):
        """check if a blob with name blob_name
        exits in bucket

        Args:
            blob_name (str): Name of blob

        Returns:
            bool : True if blob exists else False
        """
        return storage.Blob(bucket=self._bucket, name=blob_name).exists(self._sc)

    def download_file(self, src_file, dst_file):
        """Download src_file from bucket, store as dst_file

        Args:
            src_file (str): Name of blob to download
                            e.g file.tgz
            dst_file (str): Path of file to store
                            e.g /tmp/file.tgz
        """
        blob = self._bucket.blob(src_file)
        blob.download_to_filename(dst_file)

    def upload_blob(self, source_file_name, destination_bucket, destination_blob_name):
        """Uploads a file to a bucket.

        Args:
            source_file_name (str): Path of file to upload
                                    e.g /tmp/file.tgz
            destination_bucket (str): Name of bucket to upload to
            destination_blob_name (str): Name of blob to upload
                                         e.g file.tgz
        """
        upload_bucket = self._sc.get_bucket(destination_bucket)
        blob = upload_bucket.blob(destination_blob_name)
        blob.upload_from_filename(source_file_name)

    def move_blob(self, src_blob_name, dest_bucket_name, dest_blob_name):
        """
        Moves blobs between buckets or directories. Uses GCP's copy
        function then deletes the blob from the old location.

        Args:
            blob_name (str): name of file/blob to be moved
                            e.g. 'data/some_location/file_name'
            dest_bucket_name (str): name of bucket to move file/blob to
            dest_blob_name (str): name of blob to copy to dest_bucket_name
        """
        source_blob = self._bucket.blob(src_blob_name)
        destination_bucket = self._sc.get_bucket(dest_bucket_name)

        if self._bucket_name != dest_bucket_name:
            # check if blob exist in source bucket
            if self.blob_exists(src_blob_name):
                self._bucket.copy_blob(source_blob, destination_bucket, dest_blob_name)
                source_blob.delete()
